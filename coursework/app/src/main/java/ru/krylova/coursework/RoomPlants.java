package ru.krylova.coursework;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by User on 16.03.2018.
 */

public class RoomPlants extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_plants);
    }

    public void onClickNW(View view) {
        Intent intent = new Intent(RoomPlants.this, NewWindow.class);
        startActivity(intent);
    }
}
